class TemperatureDay {
  late double temperature;
  late String date;

  TemperatureDay({required this.temperature, required this.date});
}
// listTemp()=>[
//       TemperatureDay(temperature: -8.0, date: "28/05/2021"),
//       TemperatureDay(temperature: -9.0, date: "29/05/2021"),
//       TemperatureDay(temperature: -10.0, date: "30/05/2021"),
//       TemperatureDay(temperature: -12.0, date: "30/05/2021"),
//       TemperatureDay(temperature: -17.0, date: "31/05/2021"),
//       TemperatureDay(temperature: -10.0, date: "1/06/2021"),
//       TemperatureDay(temperature: -5.0, date: "2/06/2021"),
//       TemperatureDay(temperature: 5.0, date: "3/06/2021"),
//       TemperatureDay(temperature: -5.0, date: "4/06/2021"),
//       TemperatureDay(temperature: 0.0, date: "5/06/2021"),
//       TemperatureDay(temperature: -5.0, date: "6/06/2021"),
//       TemperatureDay(temperature: -8.0, date: "7/06/2021"),
//       TemperatureDay(temperature: -10.0, date: "8/06/2021"),
//       TemperatureDay(temperature: -20, date: "9/06/2021"),
//       TemperatureDay(temperature: -29, date: "10/06/2021"),
//       TemperatureDay(temperature: -24, date: "11/06/2021"),
//       TemperatureDay(temperature: -19, date: "12/06/2021"),
//       TemperatureDay(temperature: -15, date: "13/06/2021"),
//       TemperatureDay(temperature: -20, date: "14/06/2021"),
//       TemperatureDay(temperature: -22, date: "15/06/2021"),
//       TemperatureDay(temperature: -23, date: "16/06/2021"),
//       TemperatureDay(temperature: -25, date: "17/06/2021"),
//       TemperatureDay(temperature: -26, date: "18/06/2021"),
// ];

listTemp() => [
      new TemperatureDay(temperature: -8.0, date: "28/05/2021"),
      new TemperatureDay(temperature: -9.0, date: "29/05/2021"),
      new TemperatureDay(temperature: -11.0, date: "30/05/2021"),
      new TemperatureDay(temperature: -12.0, date: "30/05/2021"),
      new TemperatureDay(temperature: -13.0, date: "31/05/2021"),
      new TemperatureDay(temperature: -10.0, date: "1/06/2021"),
      new TemperatureDay(temperature: 1.0, date: "2/06/2021"),
      new TemperatureDay(temperature: 1.0, date: "3/06/2021"),
      new TemperatureDay(temperature: -1.0, date: "4/06/2021"),
      new TemperatureDay(temperature: -4.0, date: "5/06/2021"),
      new TemperatureDay(temperature: -5.0, date: "6/06/2021"),
      new TemperatureDay(temperature: -6.0, date: "7/06/2021"),
      new TemperatureDay(temperature: -10.0, date: "8/06/2021"),
      new TemperatureDay(temperature: -23, date: "9/06/2021"),
      new TemperatureDay(temperature: -25, date: "10/06/2021"),
      new TemperatureDay(temperature: -24, date: "11/06/2021"),
      new TemperatureDay(temperature: -18, date: "12/06/2021"),
      new TemperatureDay(temperature: -17, date: "13/06/2021"),
      new TemperatureDay(temperature: -20, date: "14/06/2021"),
      new TemperatureDay(temperature: -22, date: "15/06/2021"),
      new TemperatureDay(temperature: -23, date: "16/06/2021"),
      new TemperatureDay(temperature: -24, date: "17/06/2021"),
      new TemperatureDay(temperature: -26, date: "18/06/2021"),
    ];

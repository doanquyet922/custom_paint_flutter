import 'package:custom_paint/models/temperature_day.dart';
import 'package:custom_paint/temp_by_day/painter_temp_by_day.dart';
import 'package:flutter/material.dart';

class PaintTemperatureByDay extends StatefulWidget {
  const PaintTemperatureByDay({Key? key}) : super(key: key);

  @override
  _PaintTemperatureByDayState createState() => _PaintTemperatureByDayState();
}

class _PaintTemperatureByDayState extends State<PaintTemperatureByDay> {
  List<TemperatureDay> temps = listTemp();
  var selectedIndexTemp = 0;

  @override
  Widget build(BuildContext context) {
    // var distance = (265 - 2 * temps.length) / (temps.length - 1);
    // double beginX = 35;
    return MaterialApp(
      theme: ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.only(bottom: 20, left: 30, right: 20),
            child: Row(
              children: [
                Text(
                  "Nhiệt độ trung bình (°C)",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold,color: Colors.black45),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.centerRight,
                    // width: 100,
                    child: TextButton(
                        onPressed: () {},
                        child: Text(
                          "Xem chi tiết",
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        )),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            child: Stack(
              children: [
                CustomPaint(
                  size: const Size(300, 200),
                  painter: PainterTempByDay(
                      temps: temps,
                      selectedIndexDate: selectedIndexTemp,
                      widthScreen: MediaQuery.of(context).size.width),
                ),
                Positioned(
                  left: 35,
                  height: 200,
                  width: 265,
                  child: Container(
                    // color: Colors.green.withOpacity(0.5),
                    child: GestureDetector(
                      onHorizontalDragUpdate: (pos) {
                        int width = pos.globalPosition.dx.toInt();
                        setState(() {
                          selectedIndexTemp =
                              width ~/ (300 / (temps.length - 1));
                          if (selectedIndexTemp < 0) selectedIndexTemp = 0;
                          if (selectedIndexTemp > temps.length - 1)
                            selectedIndexTemp = temps.length - 1;
                        });
                      },
                      // child: ListView.separated(
                      //     physics: NeverScrollableScrollPhysics(),
                      //     separatorBuilder: (BuildContext context, int index) {
                      //       return SizedBox(
                      //         width: distance,
                      //       );
                      //     },
                      //     scrollDirection: Axis.horizontal,
                      //     itemCount: temps.length,
                      //     itemBuilder: (context, index) {
                      //       return Container(
                      //         padding: EdgeInsets.only(left: 10),
                      //         color: Colors.amber,
                      //         width: 2,
                      //         height: 200,
                      //       );
                      //     }),
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(Icons.arrow_back))
        ],
      )),
    );
  }
}

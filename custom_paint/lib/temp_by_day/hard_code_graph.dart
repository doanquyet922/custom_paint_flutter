import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class CustomPaint2 extends StatefulWidget {
  const CustomPaint2({Key? key}) : super(key: key);

  @override
  _CustomPaint2State createState() => _CustomPaint2State();
}

class _CustomPaint2State extends State<CustomPaint2> {
  var point = Offset(180, 100);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: const EdgeInsets.only(bottom: 20, left: 30, right: 20),
            child: Row(
              children: [
                Text(
                  "Nhiệt độ trung bình (°C)",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.centerRight,
                    // width: 100,
                    child: TextButton(
                        onPressed: () {},
                        child: Text(
                          "Xem chi tiết",
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        )),
                  ),
                )
              ],
            ),
          ),
          CustomPaint(size: const Size(300, 300), painter: MyPainter(point)),
          IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(Icons.arrow_back))
        ],
      )),
    );
  }
}

class MyPainter extends CustomPainter {
  List<Offset> offsets = [
    Offset(35, 90),
    Offset(70, 117),
    Offset(85, 115),
    Offset(100, 110),
    Offset(105, 75),
    Offset(110, 45),
    Offset(115, 45),
    Offset(130, 40),
    Offset(140, 59),
    Offset(160, 80),
    Offset(170, 80),
    Offset(175, 80),
    Offset(180, 100),
    Offset(180, 100),
    Offset(190, 150),
    Offset(195, 170),
    Offset(205, 170),
    Offset(210, 170),
    Offset(220, 160),
    Offset(220, 160),
    Offset(225, 150),
    Offset(230, 145),
    Offset(235, 144),
    Offset(245, 140),
    Offset(250, 144),
    Offset(260, 145),
    Offset(300, 180),
  ];
  Offset pointTouch = Offset(180, 100);
  var pathOn = Path();

  MyPainter(this.pointTouch);

  @override
  void paint(Canvas canvas, Size size) {
    var valTemperature = 10;
    var yMax = 0.0;
    for (double i = 0; i <= 40; i += 10) {
      yMax = i * 5;
      print("height:" + yMax.toString());
      TextSpan span = new TextSpan(
          text: '$valTemperature', style: TextStyle(color: Colors.black));
      TextPainter tp = new TextPainter(
          text: span,
          textAlign: TextAlign.right,
          textDirection: TextDirection.ltr)
        ..layout(maxWidth: 25, minWidth: 25);

      tp.paint(canvas, new Offset(0, yMax - 10));
      valTemperature -= 10;
      var paint = Paint()
        ..color = Color(0x40888888)
        ..strokeWidth = 1
        ..style = PaintingStyle.stroke;
      canvas.drawLine(Offset(35, yMax), Offset(size.width, yMax), paint);
    }
    print("ymax:" + yMax.toString());
    TextSpan span =
        new TextSpan(text: '28/05/2021', style: TextStyle(color: Colors.black));
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr)
      ..layout(maxWidth: 35);
    tp.layout();
    tp.paint(canvas, new Offset(35, yMax + 15));
    TextSpan span2 =
        new TextSpan(text: '28/06/2021', style: TextStyle(color: Colors.black));
    TextPainter tp2 = new TextPainter(
        text: span2,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr)
      ..layout(maxWidth: 35);
    tp2.layout();
    tp2.paint(canvas, new Offset(size.width - 70, yMax + 15));
    var path = Path();
    var path2 = Path();
    path2.moveTo(35, yMax);
    path2.lineTo(offsets[0].dx, offsets[0].dy);
    path.moveTo(offsets[0].dx, offsets[0].dy);
    for (int i = 0; i < offsets.length - 2; i += 2) {
      path.quadraticBezierTo(offsets[i + 1].dx, offsets[i + 1].dy,
          offsets[i + 2].dx, offsets[i + 2].dy);
      path2.quadraticBezierTo(offsets[i + 1].dx, offsets[i + 1].dy,
          offsets[i + 2].dx, offsets[i + 2].dy);
    }
    path2.lineTo(offsets[offsets.length - 1].dx, yMax);
    var paintG = Paint()
      ..shader = ui.Gradient.linear(
          Offset(size.width / 2, 0), Offset(size.width / 2, size.width), [
        Colors.lightBlueAccent.withOpacity(1),
        Colors.white.withOpacity(0.1),
      ]);
    path2.close();
    canvas.drawPath(path2, paintG);

    var paintLine = Paint();
    paintLine.color = Color(0xFF2962FF);
    paintLine..strokeWidth = 2;
    paintLine..style = PaintingStyle.stroke;
    // path.close();
    canvas.drawPath(path, paintLine);
    _textPoint(Offset(180, 100), canvas, yMax);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

  @override
  bool hitTest(Offset position) {
    // _path - is the same one we built in paint() method;
    return pathOn.contains(position);
  }

  _textPoint(Offset point, Canvas canvas, double yMax) {
    var paintDot = Paint();
    paintDot.color = Color(0xFF2962FF);
    canvas.drawCircle(point, 5, paintDot);

    var pathPoint = Path();
    pathPoint.moveTo(point.dx - 2, point.dy - 20);
    pathPoint.quadraticBezierTo(
        point.dx, point.dy - 10, point.dx + 2, point.dy - 20);
    pathPoint.lineTo(point.dx + 80, point.dy - 20);
    pathPoint.quadraticBezierTo(
        point.dx + 85, point.dy - 20, point.dx + 85, point.dy - 25);
    pathPoint.lineTo(point.dx + 85, point.dy - 45);
    pathPoint.quadraticBezierTo(
        point.dx + 85, point.dy - 50, point.dx + 80, point.dy - 50);
    pathPoint.lineTo(point.dx - 20, point.dy - 50);
    pathPoint.quadraticBezierTo(
        point.dx - 25, point.dy - 50, point.dx - 25, point.dy - 45);
    pathPoint.lineTo(point.dx - 25, point.dy - 25);
    pathPoint.quadraticBezierTo(
        point.dx - 25, point.dy - 20, point.dx - 20, point.dy - 20);
    pathPoint.close();
    var paintPoint = Paint()..color = Colors.black87;
    canvas.drawPath(pathPoint, paintPoint);

    TextSpan spanPoint1 = TextSpan(children: [
      TextSpan(text: "-10°C", style: TextStyle(color: Colors.white)),
      TextSpan(text: " 13/06/21", style: TextStyle(color: Color(0x80FFFFFF))),
    ]);
    TextPainter painter1 = TextPainter(
        text: spanPoint1,
        textDirection: TextDirection.ltr,
        textAlign: TextAlign.center);
    painter1.layout();
    Offset offPoint1 = Offset(point.dx - 17, point.dy - 42);
    painter1.paint(canvas, offPoint1);

    var paintLineDown = Paint();
    paintLineDown.color = Color(0xFF2962FF);
    paintLineDown..strokeWidth = 1;
    paintLineDown..style = PaintingStyle.stroke;
    // var pathLineDown = Path();
    // pathLineDown.lineTo(180, 100);
    // pathLineDown.lineTo(180, yMax);
    canvas.drawLine(point, Offset(point.dx, yMax), paintLineDown);
  }
}

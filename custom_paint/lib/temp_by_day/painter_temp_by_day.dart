import 'dart:ui' as ui;
import 'package:custom_paint/models/temperature_day.dart';
import 'package:flutter/material.dart';

class PainterTempByDay extends CustomPainter {
  int lineNumber = 5;
  double beginX = 35;
  double maxTemp = 10.0;
  double minTemp = -30;
  List<TemperatureDay> temps;
  int selectedIndexDate;
  double widthScreen;

  PainterTempByDay(
      {required this.temps,
      required this.selectedIndexDate,
      required this.widthScreen});

  @override
  void paint(Canvas canvas, Size size) {
    _drawLine(canvas, size, maxTemp, minTemp, lineNumber);
    _drawBeginEndDay(canvas, size);
    var listOffset = _listOffset(size.width, size.height);
    _drawTemp(canvas, size, listOffset);

    _textPointRight(listOffset[selectedIndexDate], temps[selectedIndexDate],
        canvas, size.height);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

  List<Offset> _listOffset(double width, double height) {
    List<Offset> list = [];
    for (int i = 0; i < temps.length; i++) {
      var dx = _convertDateToDx(width - beginX, i, temps.length);
      var dy = _convertTempToDy(height, temps[i].temperature, minTemp, maxTemp);
      list.add(Offset(dx, dy));
    }
    return list;
  }

  _drawTemp(Canvas canvas, Size size, List<Offset> listOffset) {
    var pathGradient = Path();
    pathGradient.moveTo(beginX, size.height);
    pathGradient.lineTo(listOffset[0].dx, listOffset[0].dy);

    var pathLineTemp = Path();

    // pathLineTemp.moveTo(listOffset[0].dx, listOffset[0].dy);

    // for (int i = 0; i < temps.length - 2; i += 2) {
    //   var p1 = listOffset[i];
    //   var p2 = listOffset[i + 1];
    //   var p3 = listOffset[i + 2];
    //   var controlPoint = _controlPoint(p1, p2, p3);
    //   pathLineTemp.quadraticBezierTo(
    //       controlPoint.dx, controlPoint.dy, p3.dx, p3.dy);
    //   pathGradient.quadraticBezierTo(
    //       controlPoint.dx, controlPoint.dy, p3.dx, p3.dy);
    // }

    for (int i = 0; i < listOffset.length; i++) {
      if (i == 0) {
        pathLineTemp.moveTo(listOffset[i].dx, listOffset[i].dy);
      } else {
        final prePoint = listOffset[i - 1];
        final curPoint = listOffset[i];
        final controlPoint = prePoint.dx + (curPoint.dx - prePoint.dx) * 0.5;
        pathLineTemp.cubicTo(controlPoint, prePoint.dy, controlPoint,
            curPoint.dy, curPoint.dx, curPoint.dy);
        pathGradient.cubicTo(controlPoint, prePoint.dy, controlPoint,
            curPoint.dy, curPoint.dx, curPoint.dy);
      }
    }

    var paintLineTemp = Paint();
    paintLineTemp.color = Color(0xFF2962FF);
    paintLineTemp..strokeWidth = 2;
    paintLineTemp..style = PaintingStyle.stroke;
    canvas.drawPath(pathLineTemp, paintLineTemp);

    pathGradient.lineTo(size.width, size.height);
    var paintGradient = Paint()
      ..shader = ui.Gradient.linear(
        Offset(size.width / 2, 0),
        Offset(size.width / 2, size.height),
        [
          Colors.blue.withOpacity(0.8),
          Colors.white.withOpacity(0.5),
        ],
      );
    pathGradient.close();
    canvas.drawPath(pathGradient, paintGradient);
  }

  // _controlPoint(Offset point1, Offset point2, Offset point3) {
  //   var t = (point2.dx - point1.dx) / (point3.dx - point1.dx);
  //   var dx = (point2.dx - pow(t, 2) * point3.dx - pow(1 - t, 2) * point1.dx) /
  //       (2 * (1 - t) * t);
  //   var dy = (point2.dy - pow(t, 2) * point3.dy - pow(1 - t, 2) * point1.dy) /
  //       (2 * (1 - t) * t);
  //   return Offset(dx, dy);
  // }

  _drawBeginEndDay(Canvas canvas, Size size) {
    TextSpan textSpanDayBegin = new TextSpan(
        text: '${temps[0].date}', style: TextStyle(color: Colors.black26));
    TextPainter tpDayBegin = new TextPainter(
        text: textSpanDayBegin,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr);
    tpDayBegin.layout();
    tpDayBegin.paint(canvas, new Offset(beginX, size.height + 10));
    TextSpan textSpanDayEnd = new TextSpan(
        text: '${temps[temps.length - 1].date}',
        style: TextStyle(color: Colors.black26));
    TextPainter tpDayEnd = new TextPainter(
        text: textSpanDayEnd,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr);
    tpDayEnd.layout();
    tpDayEnd.paint(
        canvas, new Offset(size.width - tpDayEnd.size.width, size.height + 10));
  }

  _convertDateToDx(double widthPaint, int indexDate, int lenghtList) {
    return widthPaint / (lenghtList - 1) * indexDate + beginX;
  }

  _convertTempToDy(
      double heightPaint, double currentTemp, double minTemp, maxTemp) {
    var temperatureRange = maxTemp - currentTemp;
    return temperatureRange * heightPaint / (maxTemp - minTemp);
  }

  _drawLine(
    Canvas canvas,
    Size size,
    double valueMax,
    double valueMin,
    int lineNumber,
  ) {
    var distanceLine = size.height / (lineNumber - 1);
    var valueFirst = valueMax;
    var distanceValue = (valueMax - valueMin) / (lineNumber - 1);
    for (int i = 0; i < lineNumber; i++) {
      var startPointLine = Offset(beginX, i * distanceLine);
      var endPointLine = Offset(size.width, i * distanceLine);
      TextSpan textSpanTemp = TextSpan(
          text: (i == 0 ? valueFirst : valueFirst -= distanceValue)
              .toInt()
              .toString(),
          style: TextStyle(color: Colors.black26));
      TextPainter tp = new TextPainter(
          text: textSpanTemp,
          textAlign: TextAlign.right,
          textDirection: TextDirection.ltr)
        ..layout(maxWidth: beginX - 7, minWidth: beginX - 7);

      tp.paint(canvas, new Offset(0, startPointLine.dy - (tp.size.height / 2)));
      var paint = Paint()
        ..color = Color(0x40888888)
        ..strokeWidth = 1
        ..style = PaintingStyle.stroke;
      canvas.drawLine(startPointLine, endPointLine, paint);
    }
  }

   _textPointRight(Offset point, TemperatureDay temperatureDay, Canvas canvas,
      double height) {



    var paintDot = Paint();
    paintDot.color = Color(0xFF2962FF);
    canvas.drawCircle(point, 5, paintDot);
    var pathPoint = Path();
    pathPoint.moveTo(point.dx - 2, point.dy - 20);
    pathPoint.quadraticBezierTo(
        point.dx, point.dy - 10, point.dx + 2, point.dy - 20);
    pathPoint.lineTo(point.dx + 80, point.dy - 20);
    pathPoint.quadraticBezierTo(
        point.dx + 85, point.dy - 20, point.dx + 85, point.dy - 25);
    pathPoint.lineTo(point.dx + 85, point.dy - 45);
    pathPoint.quadraticBezierTo(
        point.dx + 85, point.dy - 50, point.dx + 80, point.dy - 50);
    pathPoint.lineTo(point.dx - 20, point.dy - 50);
    pathPoint.quadraticBezierTo(
        point.dx - 25, point.dy - 50, point.dx - 25, point.dy - 45);
    pathPoint.lineTo(point.dx - 25, point.dy - 25);
    pathPoint.quadraticBezierTo(
        point.dx - 25, point.dy - 20, point.dx - 20, point.dy - 20);
    pathPoint.close();
    var paintPoint = Paint()..color = Colors.black87;


    TextSpan spanPoint = TextSpan(children: [
      TextSpan(
          text: "${temperatureDay.temperature}°C",
          style: TextStyle(color: Colors.white, fontSize: 11)),
      TextSpan(
          text: " ${temperatureDay.date}",
          style: TextStyle(color: Color(0x80FFFFFF), fontSize: 11)),
    ]);
    TextPainter painter = TextPainter(
        text: spanPoint,
        textDirection: TextDirection.ltr,
        textAlign: TextAlign.center);
    painter.layout();
    Offset offPoint1 = Offset(point.dx - 17, point.dy - 42);


    var d=widthScreen-painter.width;
    var dis=point.dx - 17;
    if(dis>d){
      _textPointLeft(point, temperatureDay, canvas, height);
      return;
    }else{
      canvas.drawPath(pathPoint, paintPoint);
      painter.paint(canvas, offPoint1);
      var paintLineDown = Paint();
      paintLineDown.color = Color(0xFF2962FF);
      paintLineDown..strokeWidth = 1;
      paintLineDown..style = PaintingStyle.stroke;
      canvas.drawLine(point, Offset(point.dx, height), paintLineDown);
    }



  }

   _textPointLeft(Offset point, TemperatureDay temperatureDay, Canvas canvas,
      double height) {
    var paintDot = Paint();
    paintDot.color = Color(0xFF2962FF);
    canvas.drawCircle(point, 5, paintDot);

    var pathPointLeft = Path();
    pathPointLeft.moveTo(point.dx - 2, point.dy - 20);
    pathPointLeft.quadraticBezierTo(
        point.dx, point.dy - 10, point.dx + 2, point.dy - 20);
    pathPointLeft.lineTo(point.dx + 20, point.dy - 20);
    pathPointLeft.quadraticBezierTo(
        point.dx + 25, point.dy - 20, point.dx + 25, point.dy - 25);
    pathPointLeft.lineTo(point.dx + 25, point.dy - 45);
    pathPointLeft.quadraticBezierTo(
        point.dx + 25, point.dy - 50, point.dx + 20, point.dy - 50);
    pathPointLeft.lineTo(point.dx - 80, point.dy - 50);
    pathPointLeft.quadraticBezierTo(
        point.dx - 85, point.dy - 50, point.dx - 85, point.dy - 45);
    pathPointLeft.lineTo(point.dx - 85, point.dy - 25);
    pathPointLeft.quadraticBezierTo(
        point.dx - 85, point.dy - 20, point.dx - 80, point.dy - 20);
    // pathPointLeft.close();
    var paintPoint = Paint()..color = Colors.black87;
    canvas.drawPath(pathPointLeft, paintPoint);

    TextSpan spanPoint = TextSpan(children: [
      TextSpan(
          text: "${temperatureDay.temperature}°C",
          style: TextStyle(color: Colors.white, fontSize: 11)),
      TextSpan(
          text: " ${temperatureDay.date}",
          style: TextStyle(color: Color(0x80FFFFFF), fontSize: 11)),
    ]);
    TextPainter painter = TextPainter(
        text: spanPoint,
        textDirection: TextDirection.ltr,
        textAlign: TextAlign.center);
    painter.layout();
    Offset offPoint1 = Offset(point.dx - 78, point.dy - 42);
    painter.paint(canvas, offPoint1);

    var paintLineDown = Paint();
    paintLineDown.color = Color(0xFF2962FF);
    paintLineDown..strokeWidth = 1;
    paintLineDown..style = PaintingStyle.stroke;
    canvas.drawLine(point, Offset(point.dx, height), paintLineDown);
  }
}

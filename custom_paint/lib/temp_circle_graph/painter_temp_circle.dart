import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'dart:ui' as ui;

class PainterTempCircle extends CustomPainter {
  final value;

  PainterTempCircle({
    required this.value,
  });

  final Gradient _gradient = new LinearGradient(
    colors: [Colors.lightBlueAccent, Colors.greenAccent, Colors.green],
  );

  @override
  void paint(Canvas canvas, Size size) {
    var centerX = size.width / 2;
    var centerY = size.height / 2;
    Offset center = Offset(centerX, centerY);
    var radius = math.min(centerX, centerY);
    //drawCircle
    final paint = Paint()
      ..shader = ui.Gradient.linear(
          Offset(size.width / 2, 0), Offset(size.width / 2, size.width), [
        Colors.lightBlueAccent,
        Colors.green,
      ])
      ..maskFilter = MaskFilter.blur(BlurStyle.inner, 10)
    ;
    canvas.drawCircle(center, radius, paint);
    //drawArcBehind
    final paintPa = Paint()
      ..color = Colors.black26
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 10;
    _arc(canvas, radius + 20, center, 20, paintPa);
    //drawArcCurrent
    final paintArcTemp = Paint()
      ..shader = _gradient.createShader(Rect.fromCircle(
        center: center,
        radius: radius,
      ))
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 10;
    _arc(canvas, radius + 20, center, value, paintArcTemp);
    //drawLinePointBehind
    _drawLinePointBehind(canvas, radius, center);
    //drawLinePointCurrent
    _drawLinePointCurrent(canvas, radius, center);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

  _drawLinePointCurrent(
      Canvas canvas, double radiusCircleInside, Offset center) {
    final paint2 = Paint()
      ..shader = _gradient.createShader(Rect.fromCircle(
        center: center,
        radius: radiusCircleInside,
      ))
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;
    var outerCircleRadius = radiusCircleInside + 45;
    var innerCircleRadius = radiusCircleInside + 35;

    for (int i = 150; i <= 150 + (value + 20) * 6; i += 15) {
      if (i % 30 == 0 && i % 60 != 0) {
        outerCircleRadius = radiusCircleInside + 55;
      } else
        outerCircleRadius = radiusCircleInside + 45;
      var x1 = center.dx + outerCircleRadius * math.cos(i * math.pi / 180);
      var y1 = center.dx + outerCircleRadius * math.sin(i * math.pi / 180);

      var x2 = center.dy + innerCircleRadius * math.cos(i * math.pi / 180);
      var y2 = center.dy + innerCircleRadius * math.sin(i * math.pi / 180);

      canvas.drawLine(Offset(x1, y1), Offset(x2, y2), paint2);
    }
  }

  _drawLinePointBehind(
      Canvas canvas, double radiusCircleInside, Offset center) {
    final paint3 = Paint()
      ..color = Colors.black26
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;
    var outerCircleRadius1 = radiusCircleInside + 45;
    var innerCircleRadius1 = radiusCircleInside + 35;
    for (int i = 30; i >= -210; i -= 15) {
      if (i % 30 == 0 && i % 60 != 0) {
        outerCircleRadius1 = radiusCircleInside + 55;
      } else
        outerCircleRadius1 = radiusCircleInside + 45;
      var x1 = center.dx + outerCircleRadius1 * math.cos(i * math.pi / 180);
      var y1 = center.dx + outerCircleRadius1 * math.sin(i * math.pi / 180);

      var x2 = center.dy + innerCircleRadius1 * math.cos(i * math.pi / 180);
      var y2 = center.dy + innerCircleRadius1 * math.sin(i * math.pi / 180);

      canvas.drawLine(Offset(x1, y1), Offset(x2, y2), paint3);
      if (i == 30) {
        outerCircleRadius1 = radiusCircleInside + 55;
        TextSpan span =
            new TextSpan(text: '20°C', style: TextStyle(color: Colors.black));
        TextPainter tp = new TextPainter(
            text: span,
            textAlign: TextAlign.center,
            textDirection: TextDirection.ltr);
        tp.layout();
        tp.paint(
            canvas,
            new Offset(
                center.dx +
                    (innerCircleRadius1 + 45) * math.cos(i * math.pi / 180) -
                    13,
                center.dx +
                    (innerCircleRadius1 + 30) * math.sin(i * math.pi / 180) -
                    4));
      }
      if (i == -210) {
        outerCircleRadius1 = radiusCircleInside + 55;
        TextSpan span =
            new TextSpan(text: '-20°C', style: TextStyle(color: Colors.black));
        TextPainter tp = new TextPainter(
            text: span,
            textAlign: TextAlign.center,
            textDirection: TextDirection.ltr);
        tp.layout();
        tp.paint(
            canvas,
            new Offset(
                center.dx +
                    (innerCircleRadius1 + 45) * math.cos(i * math.pi / 180) -
                    18,
                center.dx +
                    (innerCircleRadius1 + 30) * math.sin(i * math.pi / 180) -
                    4));
      }
      if (i == -30) {
        outerCircleRadius1 = radiusCircleInside + 55;
        TextSpan span =
            new TextSpan(text: '10°C', style: TextStyle(color: Colors.black));
        TextPainter tp = new TextPainter(
            text: span,
            textAlign: TextAlign.center,
            textDirection: TextDirection.ltr);
        tp.layout();
        tp.paint(
            canvas,
            new Offset(
                center.dx +
                    (innerCircleRadius1 + 45) * math.cos(i * math.pi / 180) -
                    13,
                center.dx +
                    (innerCircleRadius1 + 30) * math.sin(i * math.pi / 180) -
                    10));
      }
      if (i == -150) {
        outerCircleRadius1 = radiusCircleInside + 55;
        TextSpan span =
            new TextSpan(text: '-10°C', style: TextStyle(color: Colors.black));
        TextPainter tp = new TextPainter(
            text: span,
            textAlign: TextAlign.center,
            textDirection: TextDirection.ltr);
        tp.layout();
        tp.paint(
            canvas,
            new Offset(
                center.dx +
                    (innerCircleRadius1 + 45) * math.cos(i * math.pi / 180) -
                    20,
                center.dx +
                    (innerCircleRadius1 + 30) * math.sin(i * math.pi / 180) -
                    10));
      }
      if (i == -90) {
        outerCircleRadius1 = radiusCircleInside + 55;
        TextSpan span =
            new TextSpan(text: '0°C', style: TextStyle(color: Colors.black));
        TextPainter tp = new TextPainter(
            text: span,
            textAlign: TextAlign.center,
            textDirection: TextDirection.ltr);
        tp.layout();
        tp.paint(
            canvas,
            new Offset(
                center.dx +
                    (innerCircleRadius1 + 45) * math.cos(i * math.pi / 180) -
                    13,
                center.dx +
                    (innerCircleRadius1 + 30) * math.sin(i * math.pi / 180) -
                    12));
      }
    }
  }

  _arc(Canvas canvas, double radius, Offset center, double value, Paint paint) {
    var degrees = (value + 20) * 6;
    final rect = Rect.fromCircle(radius: radius, center: center);
    final startAngle = 5 * math.pi / 6;
    final sweepAngle = degrees * math.pi / 180;
    final useCenter = false;

    canvas.drawArc(
        rect,
        startAngle,
        sweepAngle, //radians
        useCenter,
        paint);
  }
}

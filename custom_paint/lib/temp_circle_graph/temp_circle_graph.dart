import 'dart:math' as math;
import 'package:custom_paint/temp_by_day/graph_temperature_by_day.dart';
import 'package:custom_paint/temp_circle_graph/painter_temp_circle.dart';
import 'package:flutter/material.dart';

class CustomPaintTest extends StatefulWidget {
  const CustomPaintTest({Key? key}) : super(key: key);

  @override
  State<CustomPaintTest> createState() => _CustomPaintTestState();
}

class _CustomPaintTestState extends State<CustomPaintTest>
    with SingleTickerProviderStateMixin {
  late AnimationController _tempAnimationController;
  late Animation<double> _tempAnimation;

  void setUpTempController() {
    _tempAnimationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1000));
    _tempAnimation = CurvedAnimation(
        parent: _tempAnimationController, curve: Interval(0.0, 1.0));
  }

  @override
  void initState() {
    // TODO: implement initState
    setUpTempController();

    super.initState();
  }

  @override
  void dispose() {
    _tempAnimationController.dispose(); // TODO: implement dispose
    super.dispose();
  }

  var curTemp = 10.0;
  var preTemp = 10.0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: AnimatedBuilder(
        animation: _tempAnimationController,
        builder: (context, _) {
          return Scaffold(
              body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 400,
                width: 400,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child:
                          //[-20,10]
                          CustomPaint(
                        size: const Size(190, 190),
                        painter: PainterTempCircle(
                            value: preTemp +
                                (curTemp - preTemp) * _tempAnimation.value),
                      ),
                    ),
                    Text(
                      "${double.parse(curTemp.toStringAsFixed(1))}°C",
                      style: TextStyle(fontSize: 40, color: Colors.white),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          preTemp = curTemp;
                          curTemp = -20 + math.Random().nextDouble() * 40;
                          _tempAnimationController.reset();
                          _tempAnimationController.forward();
                        });
                      },
                    )
                  ],
                ),
              ),
              IconButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => PaintTemperatureByDay()));
                  },
                  icon: Icon(Icons.next_plan))
            ],
          ));
        },
      ),
    );
  }
}
